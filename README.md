# A ionic starter application

##Includes
-   iOS and Android as platforms
-   Simple routing
-   global settings for application
-   localStorage example
-   sass for styling
-   sample controllers
-   sample service

## Get started

Change the application **name** in
-   ionic.project
-   package.json
-   config.xml.
Follow the instructions at http://ionicframework.com/getting-started/ to get
ionic and cordova started.

Before serving the application (meaning running it in the browser), run
```
npm install
bower install
```
to get all the latest dependencies.

## Testing in browser
Ionic have a neat feature which lets you run both iOS and Android versions of your
application (if they are added as platforms) by using the Ionic Labs (-l):

```
ionic serve -l
```

## Add/remove platforms
In order to deploy the application for different platforms (iOS, Android, Windows Phone etc.)
you have to add the platforms to the project.

```
ionic platform add ios
ionic platform add android
```

## Adding plugins from Cordova
Cordova lets you access the hardware of the phone. First of all, you'll need
ngCordova (http://ngcordova.com/), which makes working with Cordova & AngularJS
even more epic. I suggest using bower for frontend dependencies and ngCordova
has already been added to this project in Bower.

To add plugins, search for your desired plugin at https://cordova.apache.org/plugins/
and follow the instructions there. It should not be really that hard.

### Interesting plugins
-   Camera access https://www.npmjs.com/package/cordova-plugin-camera
-   Geolocation https://www.npmjs.com/package/cordova-plugin-geolocation

## Tips
Use icons from http://ionicons.com/.
